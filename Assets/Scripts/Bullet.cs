﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float bulletSpeed = 1024;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //
        transform.Translate(0, bulletSpeed * Time.deltaTime, 0);

        //
        if (transform.position.y > 700)
        {
            GameObject.Destroy(gameObject);
        }
    }
}
