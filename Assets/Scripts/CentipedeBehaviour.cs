﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeBehaviour : MonoBehaviour {

    public float movementSpeed = 256f;
    public bool movingDown = false;
    public float moveDownDistance = 15;
	public AudioClip  deathSound;
	public AudioSource enemyDeath;
	

    public GameObject treePrefab;

    // Use this for initialization
    void Start () {

		
	}
	private void Awake()
	{
		enemyDeath = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {

        //
        if (Static.startGame)
        {

            //
            if (!movingDown)
            {
                transform.Translate(movementSpeed * Time.deltaTime, 0, 0);
            }

            // right wall
            if (transform.position.x >= 784 && movementSpeed > 0)
            {
                Reflect();
            }

            // left wall
            if (transform.position.x <= 16 && movementSpeed < 0)
            {
                Reflect();
            }
        }
	}

    //
    void Reflect()
    {
        movingDown = true;
        movementSpeed *= -1;
        StartCoroutine("MoveDown");
    }

    //
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bounce")
        {
            Reflect();
        }

        if (other.tag == "Bullet")
        {
            GameObject.Instantiate(treePrefab, transform.position, Quaternion.identity);
            GameObject.Destroy(other.gameObject,1);
            GameObject.Destroy(gameObject,1);
			enemyDeath.PlayOneShot(deathSound);
			

        }
    }
    //
    IEnumerator MoveDown()
    {
        float destinationY = (transform.position.y - moveDownDistance);
        do
        {
            do
            {
                transform.Translate(0, -256 * Time.deltaTime, 0);
                yield return null;
            } while (transform.position.y > destinationY);
            do
            {
                transform.Translate(0, destinationY - transform.position.y, 0);
                yield return null;
            } while ((transform.position.y != destinationY));
            destinationY = (transform.position.y - moveDownDistance);
            movingDown = false;
        } while (movingDown);
    }
}
