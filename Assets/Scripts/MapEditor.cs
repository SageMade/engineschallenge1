﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CentipedeCore;

public class MapEditor : MonoBehaviour {

    [Header("Spawn Settings")]
    public Transform SpawnRoot;
    public GameObject PlayerPrefab;
    public GameObject TreePrefab;
    public GameObject SpiderPrefab;
    public GameObject CentipedePrefab;
    
    [Header("UI Settings")]
    public Camera MainCamera;

    private GameObject[] myTilePrefabs;

    private TileMap myMap;
    private GameObject myPlayer;

    private GameObject myEditingObject;
    private TileID     myEditingID;

    public bool IsPlacingMultiple { get; set; }

    private EditorPanelController myPanelController;

    // Use this for initialization
    void Start () {
        myTilePrefabs = new GameObject[5];
        myTilePrefabs[(byte)TileID.Player   ] = PlayerPrefab;
        myTilePrefabs[(byte)TileID.Tree     ] = TreePrefab;
        myTilePrefabs[(byte)TileID.Spider   ] = SpiderPrefab;
        myTilePrefabs[(byte)TileID.Centipede] = CentipedePrefab;

        myPanelController = GetComponent<EditorPanelController>();

        myMap = new TileMap();
    }
	
	// Update is called once per frame
	void Update () {

        //
        if (myEditingObject != null) {
            Vector3 mousePos = MainCamera.ScreenToWorldPoint(Input.mousePosition) - new Vector3(16.0f, 16.0f);
            mousePos /= 32.0f;
            mousePos.x = Mathf.RoundToInt(mousePos.x);
            mousePos.y = Mathf.RoundToInt(mousePos.y);
            mousePos.z = Mathf.RoundToInt(mousePos.z);

            Vector3 finalPos = mousePos + new Vector3(0.5f, 0.5f);
            finalPos *= 32.0f;
            finalPos.z = 0.0f;
            //finalPos += new Vector3(16.0f, 16.0f, 16.0f);

            myEditingObject.transform.position = finalPos;

            if (Input.GetMouseButtonDown(0))
            {
                myMap.Set((ushort)mousePos.x, (ushort)mousePos.y, myEditingID);
                __ReconstructMap();

                if (IsPlacingMultiple) {
                    myEditingObject = Instantiate(myEditingObject, SpawnRoot, true);
                } else {
                    myEditingID = TileID.None;
                    myEditingObject = null;
                    myPanelController.SetEditorVisible(true);
                }
            }
            else if (Input.GetMouseButtonDown(1))
            {
                Destroy(myEditingObject);
                myEditingObject = null;
                myPanelController.SetEditorVisible(true);
            }
        }
        else if (Input.GetMouseButtonDown(1))
        {
            Vector3 mousePos = MainCamera.ScreenToWorldPoint(Input.mousePosition) - new Vector3(16.0f, 16.0f);
            mousePos /= 32.0f;
            mousePos.x = Mathf.RoundToInt(mousePos.x);
            mousePos.y = Mathf.RoundToInt(mousePos.y);
            mousePos.z = Mathf.RoundToInt(mousePos.z);

            myMap.Set((ushort)mousePos.x, (ushort)mousePos.y, TileID.None);
            __ReconstructMap();
        }

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Z)) {
            Undo();
        }

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Y)) {
            Redo();
        }
    }

    private void __ReconstructMap()
    {
        // Remove ALL the entities in the scene (I know, it sucks :[)
        foreach(Transform child in SpawnRoot) {
            Destroy(child.gameObject);
        }

        for(ushort x = 0; x < 25; x++) {
            for (ushort y = 0; y < 25; y++) {
                TileID id = myMap.Get(x, y);
                GameObject instance = null;
                switch (id)
                {
                    case TileID.Player:
                        instance = Instantiate(PlayerPrefab, SpawnRoot, true);
                        break;
                    case TileID.Spider:
                        instance = Instantiate(SpiderPrefab, SpawnRoot, true);
                        break;
                    case TileID.Tree:
                        instance = Instantiate(TreePrefab, SpawnRoot, true);
                        break;
                    case TileID.Centipede:
                        instance = Instantiate(CentipedePrefab, SpawnRoot, true);
                        break;
                }

                if (instance != null)
                    instance.transform.position = new Vector3((x + 0.5f) * 32.0f, (y + 0.5f) * 32.0f);
            }
        }
    }

    //
    public void PlacePlayer() {
        if (myPlayer != null) {
            Destroy(myPlayer);
        }
        myPlayer = Instantiate(PlayerPrefab, SpawnRoot, true);
        myEditingObject = myPlayer;
        myEditingID = TileID.Player;
        myPanelController.SetEditorVisible(false);
    }

    //
    public void PlaceCentipede() {
        myEditingObject = Instantiate(CentipedePrefab, SpawnRoot, true);
        myEditingID = TileID.Centipede;
        myPanelController.SetEditorVisible(false);
    }

    public void PlaceSpider() {
        myEditingObject = Instantiate(SpiderPrefab, SpawnRoot, true);
        myEditingID = TileID.Spider;
        myPanelController.SetEditorVisible(false);
    }

    //
    public void PlaceTree() {
        myEditingObject = Instantiate(TreePrefab, SpawnRoot, true);
        myEditingID = TileID.Tree;
        myPanelController.SetEditorVisible(false);
    }

    public void Undo()
    {
        myMap.Undo();
        __ReconstructMap();
    }

    public void Redo()
    {
        myMap.Redo();
        __ReconstructMap();
    }
}
