﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCore : MonoBehaviour {

    public float playerSpeed = 512;

    public GameObject bulletPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Static.startGame)
        {
            //
            if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < 784)
            {
                transform.Translate(playerSpeed * Time.deltaTime, 0, 0);
            }
            else if (Input.GetKey(KeyCode.LeftArrow) && transform.position.x > 16)
            {
                transform.Translate(-playerSpeed * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.UpArrow) && transform.position.y < 166)
            {
                transform.Translate(0, playerSpeed * Time.deltaTime, 0);
            }
            else if (Input.GetKey(KeyCode.DownArrow) && transform.position.y > 16)
            {
                transform.Translate(0, -playerSpeed * Time.deltaTime, 0);
            }

            // Shoot
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject.Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            }
        }
    }

    //
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            // dead player
            GameObject.Destroy(gameObject);
        }
    }
}
