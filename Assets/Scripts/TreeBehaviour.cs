﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeBehaviour : MonoBehaviour
{

    private int hp = 3;
	public AudioClip destroySound;
	public AudioSource Treedeath;

	// Use this for initialization
	void Start()
    {

    }
	private void Awake()
	{
		Treedeath = GetComponent<AudioSource>();
	}
	// Update is called once per frame
	void Update()
    {
        if (hp <= 0)
        {
            GameObject.Destroy(gameObject);
        }
    }

    //
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            hp--;
            GameObject.Destroy(other.gameObject,1);
            // Kill a piece of the tree
            GameObject.Destroy(transform.GetChild(0).gameObject,1);
			Treedeath.PlayOneShot(destroySound);
        }
    }
}
