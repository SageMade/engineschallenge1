#include "Commands.h"
#include "Logger.h"

/*
	Implementation of the core commands system
	Developed by Shawn
*/

void PlaceCommand::Execute(TileMap* map) {
	OldTileID = map->GetTile(TileX, TileY);
	map->SetTile(TileX, TileY, TileID);
	FILE_LOG(logINFO) << "[Place] Setting tile @ " << TileX << "," << TileY << " from " << (int)OldTileID << " to " << (int)TileID;
}

void PlaceCommand::Undo(TileMap* map) {
	map->SetTile(TileX, TileY, OldTileID);
	FILE_LOG(logINFO) << "[Place] Setting tile @ " << TileX << "," << TileY << " to " << (int)OldTileID;
}

void DeleteCommand::Execute(TileMap* map) {
	OldTileID = map->GetTile(TileX, TileY);
	map->SetTile(TileX, TileY, 0);
	FILE_LOG(logINFO) << "[Delete] Setting tile @ " << TileX << "," << TileY << " from " << (int)OldTileID << " to 0";
}

void DeleteCommand::Undo(TileMap* map) {
	map->SetTile(TileX, TileY, OldTileID);
	FILE_LOG(logINFO) << "[Delete] Setting tile @ " << TileX << "," << TileY << " to " << (int)OldTileID;
}
