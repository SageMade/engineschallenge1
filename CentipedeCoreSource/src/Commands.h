#pragma once

#include <cstdint>
#include "TileMap.h"

/*

	Definition of the core commands system
	Developed by Shawn

*/

class ICommand {
public:
	virtual void Execute(TileMap* map) = 0;
	virtual void Undo(TileMap* map) = 0;
};

class PlaceCommand : public ICommand {
public:
	void Execute(TileMap* map);
	void Undo(TileMap* map);
	uint16_t TileX, TileY;
	uint8_t TileID, OldTileID;
};

class DeleteCommand : public ICommand {
public:
	void Execute(TileMap* map);
	void Undo(TileMap* map);
	uint16_t TileX, TileY;
	uint8_t OldTileID;
};