#include "HistoryContext.h"

#include <memory>
#include "Logger.h"

/*
	Implementation of the core commands system
	Developed by Shawn
*/

HistoryContext::HistoryContext() {
	myMinId = 0;
	myMinId = 0;
	myCurrentId = 0;
	myNextId = 1;
	memset(myCommandPool, 0, sizeof(size_t) * MAX_HISTORY);
}

HistoryContext::~HistoryContext() {
	for (int ix = 0; ix < MAX_HISTORY; ix++) {
		if (myCommandPool[ix] != nullptr)
			delete myCommandPool[ix];
	}
}

void HistoryContext::Push(ICommand * command, TileMap* map) {
	command->Execute(map);
	
	if (__GetCommand(myCurrentId + 1) != nullptr) {
		delete __GetCommand(myCurrentId + 1);
	}

	__GetCommand(myCurrentId + 1) = command;
	myCurrentId++;
	myMaxId = myCurrentId;
	FILE_LOG(logINFO) << "Performed command (" << myMinId << "/" << myCurrentId << "/" << myMaxId << ")";
}

bool HistoryContext::Pop(TileMap* map)
{
	if (myCurrentId > myMinId) {
		ICommand* cmd = __GetCommand(myCurrentId);
		myCurrentId--;
		cmd->Undo(map);
		FILE_LOG(logINFO) << "Undid command (" << myMinId << "/" << myCurrentId << "/" << myMaxId << ")";
		return true;
	}
	else
		return false;
}

bool HistoryContext::Undo(TileMap* map) {
	return Pop(map);
}

bool HistoryContext::Redo(TileMap* map) {
	if (myCurrentId < myMaxId) {
		myCurrentId++;
		ICommand* cmd = __GetCommand(myCurrentId);
		cmd->Execute(map);
		FILE_LOG(logINFO) << "Redid command (" << myMinId << "/" << myCurrentId << "/" << myMaxId << ")";
		return true;
	}
	else
		return false;
}

ICommand*& HistoryContext::__GetCommand(int offset)
{
	return myCommandPool[offset % MAX_HISTORY];
}
