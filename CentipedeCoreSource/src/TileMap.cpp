#include "TileMap.h"

/*
	Implementation of the core tile map system
	Developed by Shawn and Matthew
*/

TileMap::TileMap() {
	for (int i = 0; i < 25; i++) {
		for (int t = 0; t < 20; t++) {
			id[i][t] = 0;
		}
	}
}

void TileMap::SetTile(uint16_t xPos, uint16_t yPos, uint8_t ID) {
	id[xPos][yPos] = ID;

}

uint8_t TileMap::GetTile(uint16_t xPos, uint16_t yPos) {
	return id[xPos][yPos];
}