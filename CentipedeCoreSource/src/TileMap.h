#pragma once
#include <cstdint>
#include <iostream>

/*
	Definition of the core tile map system
	Developed by Shawn and Matthew
*/

class TileMap {
public:
	TileMap();

	void SetTile(uint16_t x, uint16_t y, uint8_t id);/*{
		std::cout << "X: " << x << ", Y: " << ", Set to: " << id << std::endl;
	}*/
	uint8_t GetTile(uint16_t x, uint16_t y);
	/*{
		uint8_t result = 1;
		std::cout << "X: " << x << ", Y: " << ", Is: " << result << std::endl;
		return result;
	}*/

private:
	uint8_t  id[25][20];
};