/*
	Definitions for the exported functions
*/
#pragma once

#include "LibSettings.h"
#include "TileMap.h"
#include "HistoryContext.h"

#ifdef __cplusplus
extern "C" {
#endif
	/*
		Sets the file path for the logging output
		@param fileName The name of the file to save the logs to
	*/
	LIB_API void SetLogFileLoc(const char* fileName);
	/*
		Send text to the plugin's log file, at the given level
		@param level The LogLevel to send the message at
		@param text The text to send to the log
	*/
	LIB_API void NativeLog(int level, const char* text);

	/*
		Creates a new history context instance
	*/
	LIB_API HistoryContext* CreateHistory();

	/*
		Deletes a history context instance
		@param history The context to delete
	*/
	LIB_API void DeleteHistory(HistoryContext* history);

	LIB_API TileMap* CreateMap();

	LIB_API void DeleteMap(TileMap* map);

	LIB_API void SetTile(TileMap* map, HistoryContext* history, uint16_t x, uint16_t y, uint8_t tileID);

	LIB_API uint8_t GetTile(TileMap* map, HistoryContext* history, uint16_t x, uint16_t y);

	LIB_API void Undo(TileMap* map, HistoryContext* history);

	LIB_API void Redo(TileMap* map, HistoryContext* history);

#ifdef __cplusplus
}
#endif