/*
	Author: Shawn Matthews
	Date:   September 24, 2018
	Description:
		A test program to ensure functionality, only built in debug mode.
*/
#ifdef _DEBUG 

#include <cstdio>
#include <iostream>
#include <chrono>

#include "TileMap.h"
#include "HistoryContext.h"
#include "Wrapper.h"

// Main entry point for the debugging progam
int main() {

	SetLogFileLoc("logs.txt");
	
	HistoryContext* history = new HistoryContext();
	TileMap* map = new TileMap();

	PlaceCommand* placeCommands = new PlaceCommand[2];
	placeCommands[0].TileX = 4;
	placeCommands[0].TileY = 9;
	placeCommands[0].TileID = 2;

	placeCommands[1].TileX = 3;
	placeCommands[1].TileY = 2;
	placeCommands[1].TileID = 4;

	DeleteCommand* deleteCommnads = new DeleteCommand[2];
	deleteCommnads[0].TileX = 5;
	deleteCommnads[0].TileY = 4;

	deleteCommnads[1].TileX = 3;
	deleteCommnads[1].TileY = 2;

	history->Push(placeCommands + 0, map);
	history->Push(placeCommands + 1, map);
	history->Push(deleteCommnads + 0, map);
	history->Push(deleteCommnads + 1, map);

	std::cout << history->Undo(map) << std::endl;
	std::cout << history->Undo(map) << std::endl;
	std::cout << history->Redo(map) << std::endl;	
	history->Push((ICommand*)(deleteCommnads + 1), map);
	std::cout << history->Undo(map) << std::endl;
	std::cout << history->Undo(map) << std::endl;

}

#endif //  DEBUG